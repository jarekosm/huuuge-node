// import express from 'express'
import http from 'http'
import fs from 'fs'
import path from 'path'
import { promisify, callbackify } from 'util'
import { EventEmitter } from 'events'


http.createServer(async (req, res) => {
  const readEvents = new EventEmitter()
  const writeEvents = new EventEmitter()

  readEvents
    .on('data', chunk => writeEvents.emit('data', chunk.toString().toUpperCase()))
    .on('end', () => writeEvents.emit('end'))

  writeEvents
    .on('data', chunk => res.write(chunk))
    .on('end', () => res.end())


  let i = 0;
  setInterval(() => {
    const chunk = Buffer.from('Ala ma kota ' + i + '\n\r')
    i++
    if (i < 1000) {
      readEvents.emit('data', chunk)
    } else {
      readEvents.emit('end')
    }
  }, 10)

})
  .listen(8080);

// const fd = await fs.promises.open('./output/bigfile.txt', 'r')
// const chunk = Buffer.alloc(1024, '')
// let pos = 0;
// const { bytesRead } = await fd.read(chunk, 0, chunk.length)
// bytesRead > 0


// await combineFiles(res)
async function combineFiles(res: http.ServerResponse) {
  try {
    const nodes = await fs.promises.readdir('./output/products/')

    for (let node of nodes) {
      const file = await fs.promises.readFile(`./output/products/${node}/description.md`)

      const headerStart = file.indexOf('# ', 0)
      const headerEnd = file.indexOf('\n', headerStart)
      const output = file.slice(headerStart !== -1 ? headerEnd : 0)

      res.write(output)
    }

  }
  catch (e) {
    // 
  }
  finally {
    //
  }
  res.end()
}

